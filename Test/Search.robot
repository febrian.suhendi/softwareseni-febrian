*** Settings ***
Library  SeleniumLibrary
Library  String

*** Variables ***
${baseUrl}  https://www.softwareseni.co.id/

${div_searchButton}  //div[@class='navbar-inner-wrapper light']//div[@class='search-button-wrapper']
${input_search}  //input[@name='query']
${button_search}  //input[@value='Cari']
${h1_pageTitle}  //h1[@class='heading-24']
${section_footer}  //section[@class='footer-section wf-section']
${anchor_footerLogo}  //a[@class='link-block-17 w-inline-block']

*** Keywords ***
Open Web Application
    Open Browser  ${baseUrl}  chrome
    Maximize Browser Window
    Wait Until Location Is  ${baseUrl}  30
    Location Should Be  ${baseUrl}

Search By Keyword
    [Arguments]  ${keyword}
    ${keywordUrl}=  Replace String  ${keyword}  ${SPACE}  +
    ${searchUrl}=  Set Variable  search?query=${keywordUrl}
    ${anchor_searchResult}=  Set Variable  //div[@class='search-result-items']//a[contains(., '${keyword}')]
    
    # search keyword
    Wait Until Element Is Visible  ${div_searchButton}  30
    Click Element  ${div_searchButton}
    Wait Until Element Is Visible  ${input_search}  5
    Input Text  ${input_search}  ${keyword}
    Click Element  ${button_search}
    # verify search keyword
    Wait Until Location Is  ${baseUrl}${searchUrl}  60
    Location Should Be  ${baseUrl}${searchUrl}
    Wait Until Element Is Visible  ${anchor_searchResult}  30
    Element Should Be Visible  ${anchor_searchResult}
    # open detail
    Click Element  ${anchor_searchResult}
    # verify detail open
    Wait Until Element Is Visible  ${h1_pageTitle}  30
    Element Should Be Visible  ${h1_pageTitle}

Scroll Down Page And Back To Homepage
    # scroll to footer
    Scroll Element Into View  ${section_footer}
    # verify scroll success
    Wait Until Element Is Visible  ${section_footer}  5
    Element Should Be Visible  ${section_footer}
    # go to homepage
    Wait Until Element Is Visible  ${anchor_footerLogo}  5
    Click Element  ${anchor_footerLogo}
    # verify back to homepage
    Wait Until Location Is  ${baseUrl}  30
    Location Should Be  ${baseUrl}
    Wait Until Element Is Visible  ${div_searchButton}  30
    Element Should Be Visible  ${div_searchButton}

*** Test Cases ***
Softwareseni QA Test
    Open Web Application
    # search for angkasa pura
    Search By Keyword  Angkasa Pura - SSO
    Scroll Down Page And Back To Homepage
    # search for tim kami
    Search By Keyword  Tim Kami
    Scroll Down Page And Back To Homepage